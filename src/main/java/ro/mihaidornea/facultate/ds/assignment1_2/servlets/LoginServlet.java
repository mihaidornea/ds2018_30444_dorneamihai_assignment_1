package ro.mihaidornea.facultate.ds.assignment1_2.servlets;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Configuration;
import ro.mihaidornea.facultate.ds.assignment1_2.dao.UserDao;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.User;
import ro.mihaidornea.facultate.ds.assignment1_2.utils.CryptoUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    private UserDao userDao;
    private static final String DOCTYPE = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";

    private static final String WRONG_USERNAME = "wrongUsername";
    private static final String WRONG_PASSWORD = "wrongPassword";

    @Override
    public void init() throws ServletException {
        userDao = new UserDao(new Configuration().configure().buildSessionFactory());
        super.init();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        String role = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("role"))
                    role = cookie.getValue();
            }
        }
        if (role != null)
            redirect(role, response);
        PrintWriter printWriter = response.getWriter();
        printWriter.println(DOCTYPE);
        printWriter.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <LINK rel = \"stylesheet\" href = \"css/forms.css\" type = \"text/css\">\n" +
                "    <LINK rel = \"stylesheet\" href = \"css/login.css\" type = \"text/css\">\n" +
                "    <title>Login</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "\n" +
                "    <form id=\"loginForm\" method=\"post\" action=\"/login\">\n" +
                "\n" +
                "        <H1>Login</H1>\n" +
                "\n" +
                "        <label for = \"loginUsername\"> Username </label>\n" +
                "            <input id = \"loginUsername\" type = \"text\" name = \"loginUsername\">\n" +
                "\n" +
                "        <label for = \"loginPassword\"> Password </label>\n" +
                "            <input id = \"loginPassword\" type = \"password\" name = \"loginPassword\">\n" +
                "\n" +
                "        <div class=\"button\">\n" +
                "            <input id=\"loginButton\" type=\"submit\" name=\"loginButton\">\n" +
                "        </div>\n" +
                "    <div class=\"checkbox\">\n" +
                "            <label>Remember me<input id = \"rememberMeCheck\" type=\"checkbox\" name=\"rememberMeCheck\"></label>\n" +
                "    </div>\n" +
                "    </form>\n");

        if (request.getAttribute("Failure") != null) {
            switch ((String) request.getAttribute("Failure")) {
                case WRONG_PASSWORD:
                    printWriter.println("<div \"failure\"><p> Wrong password! </p> </div>");
                    break;
                case WRONG_USERNAME:
                    printWriter.println("<div \"failure\"><p> Wrong username! </p> </div>");
                    break;
                default:
                    break;
            }
        }
        printWriter.println("</body>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("loginUsername");
        User user = userDao.findUser(username);
        if (user != null) {
            if (user.getPassword().equals(CryptoUtils.encode(request.getParameter("loginPassword")))){
                if (request.getParameter("rememberMeCheckBox") != null) {
                    Cookie usernameCookie = new Cookie("username", user.getUsername());
                    Cookie roleCookie = new Cookie("role", user.getRole());
                    usernameCookie.setMaxAge(60 * 60 * 24);
                    roleCookie.setMaxAge(60 * 60 * 24);
                    response.addCookie(usernameCookie);
                    response.addCookie(roleCookie);
                } else {
                    HttpSession session = request.getSession();
                    session.setAttribute("username", user.getUsername());
                    session.setAttribute("role", user.getRole());
                }
                redirect(user.getRole(), response);
            } else {
                request.setAttribute("Failure", WRONG_PASSWORD);
                doGet(request,response);
            }
        } else {
            request.setAttribute("Failure", WRONG_USERNAME);
            doGet(request,response);
        }
    }

    private void redirect(String role, HttpServletResponse response) throws IOException {
        switch (role){
            case "ADMIN":
                response.sendRedirect("/admin");
                break;

            case "USER":
                response.sendRedirect("/user");
                break;
            default:
                break;
        }
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
