package ro.mihaidornea.facultate.ds.assignment1_2.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.User;
import ro.mihaidornea.facultate.ds.assignment1_2.utils.CryptoUtils;

import java.util.List;

public class UserDao {

    private static final Log LOGGER = LogFactory.getLog(UserDao.class);
    private SessionFactory sessionFactory;

    public UserDao(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public User addUser(User user){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            user.setPassword(CryptoUtils.encode(user.getPassword()));
            int userId = (int) session.save(user);
            user.setId(userId);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public User findUser(int id){
        Session session = sessionFactory.openSession();
        List<User> userList = null;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE id = :id");
            query.setParameter("id", id);
            userList = query.list();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return userList != null && !userList.isEmpty() ? userList.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public User findUser(String username){
        Session session = sessionFactory.openSession();
        List<User> userList = null;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username");
            query.setParameter("username", username);
            userList = query.list();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return userList != null && !userList.isEmpty() ? userList.get(0) : null;
    }

    public void deleteUser(int id){
        deleteUser(findUser(id));
    }

    public void deleteUser(String username){
        deleteUser(findUser(username));
    }

    public void deleteAllUsers(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.createSQLQuery("DELETE FROM User").executeUpdate();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
    }

    private void deleteUser(User user2) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            User user = user2;
            session.delete(user);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<User> listUsers(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            users = session.createQuery("FROM User").list();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return users;
    }
}
