package ro.mihaidornea.facultate.ds.assignment1_2.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.City;

import java.util.List;

public class CityDao {

    private static final Log LOGGER = LogFactory.getLog(CityDao.class);

    private SessionFactory sessionFactory;

    public CityDao(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public City addCity(City city){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            int cityId = (Integer) session.save(city);
            city.setId(cityId);
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return city;
    }

    @SuppressWarnings("unchecked")
    public City findCityById(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE id = :id");
            query.setParameter("id", id);
            cities = query.list();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public City findCityByName(String name){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE name = :name");
            query.setParameter("name", name);
            cities = query.list();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }


    public void deleteCity(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            City city = findCityById(id);
            assert city != null;
            session.delete(city);
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("" , ex);
        } finally {
            session.close();
        }
    }

    public void deleteAllCities(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.createSQLQuery("DELETE FROM City").executeUpdate();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<City> listCities(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            cities = session.createQuery("FROM City").list();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return cities;
    }
}
