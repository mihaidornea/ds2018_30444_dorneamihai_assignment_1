package ro.mihaidornea.facultate.ds.assignment1_2.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.Flight;

import java.util.List;

public class FlightDao {

    private static final Log LOGGER = LogFactory.getLog(Flight.class);

    private SessionFactory sessionFactory;

    public FlightDao(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public Flight addFlight(Flight flight){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            int flightId = (int) session.save(flight);
            flight.setId(flightId);
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return flight;
    }

    @SuppressWarnings("unchecked")
    public Flight findFlightByFlightNumber(int flightNumber){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE flightNumber = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;

    }

    @SuppressWarnings("unchecked")
    public Flight findFlight(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public List<Flight> listFlights(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
        return flights;
    }

    public void deleteFlight(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Flight flight = findFlight(id);
            session.delete(flight);
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
    }

    public void deleteFlightWithFlightNumber(int flightNumber){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Flight flight = findFlightByFlightNumber(flightNumber);
            session.delete(flight);
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
    }

    public void deleteAllFlights(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.createSQLQuery("DELETE FROM Flight").executeUpdate();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
    }

    public void updateFlight(Flight flight){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createSQLQuery("UPDATE Flight SET flightNumber = :flightNumber, " +
                    "airplaneType = :airplaneType, " +
                    "departureCity = :departureCity, " +
                    "arrivalCity = :arrivalCity, " +
                    "departureDate = :departureDate, " +
                    "arrivalDate = :arrivalDate WHERE id = :id");
            query.setParameter("flightNumber", flight.getFlightNumber());
            query.setParameter("airplaneType", flight.getAirplaneType());
            query.setParameter("departureCity", flight.getDepartureCity().getId());
            query.setParameter("arrivalCity", flight.getArrivalCity().getId());
            query.setParameter("departureDate", flight.getDepartureDate());
            query.setParameter("arrivalDate", flight.getArrivalDate());
            query.setParameter("id", flight.getId());
            query.executeUpdate();
            tx.commit();
        } catch (HibernateException ex){
            if (tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
    }
}
