package ro.mihaidornea.facultate.ds.assignment1_2.servlets;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.cfg.Configuration;
import ro.mihaidornea.facultate.ds.assignment1_2.dao.CityDao;
import ro.mihaidornea.facultate.ds.assignment1_2.dao.FlightDao;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.City;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.Flight;
import ro.mihaidornea.facultate.ds.assignment1_2.utils.HtmlUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;

public class AdminServlet extends HttpServlet {

    private static final String DOCTYPE = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
    private static final String ADMIN_ROLE = "ADMIN";
    private FlightDao flightDao;

    @Override
    public void init() throws ServletException {
        flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
        super.init();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter printWriter = response.getWriter();
        if (HtmlUtils.hasAppropriateRole(request, ADMIN_ROLE)) {
            printWriter.println(DOCTYPE);
            printWriter.println("<html>\n" +
                    "<head>" + "<meta charset=\"UTF-8\">\n" +
                    "    <LINK rel = \"stylesheet\" href = \"css/admin.css\" type = \"text/css\">\n" +
                    "    <LINK rel = \"stylesheet\" href = \"css/forms.css\" type = \"text/css\">\n" +
                    "    <title>Administration Page</title>" +
                    "</head>" +
                    "<body>");
            displayAdministrationForms(printWriter);
            if (request.getParameter("findFlightNumber") != null) {
                int flightNumber = Integer.parseInt(request.getParameter("findFlightNumber"));
                Flight flight = flightDao.findFlightByFlightNumber(flightNumber);
                if (flight != null) {
                    Date departureDate = new Date(flight.getDepartureDate().getTime());
                    Date arrivalDate = new Date(flight.getArrivalDate().getTime());
                    printWriter.println(
                            "<H2> Flight no: " + flight.getFlightNumber() + "\n" + "</H2>" +
                                    "<b>Departure City:</b> " + "<a>" + flight.getDepartureCity().getName() + "<a>" + " : " + departureDate.toString() + "<br/>\n" +
                                    "<b>Arrival City:</b> " + "<a>" + flight.getArrivalCity().getName() + "<a>" + " : " + arrivalDate.toString() + "<br/>\n" +
                                    "<b>Airplane Type:</b> " + flight.getAirplaneType() + "<br/>\n"
                    );
                } else {
                    printWriter.println("<H2> No flights were found with that flight number </H2>");
                }
            }
            printWriter.println("</body>" +
                    "</html>");
        } else {
            HtmlUtils.printErrorPage(printWriter);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("deleteFlight") != null) {
            deleteFlight(request, response);
        } else if (request.getParameter("createFlight") != null) {
            createFlight(request, response);
        } else if (request.getParameter("updateFlight") != null) {
            updateFlight(request, response);
        }
    }

    private void createFlight(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter printWriter = response.getWriter();
        printWriter.println(DOCTYPE);
        printWriter.println("<html>\n" +
                "<head>" +
                "</head>" +
                "<body>");
        CityDao cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
        City departureCity = cityDao.findCityByName(request.getParameter("createDepartureCity"));
        City arrivalCity = cityDao.findCityByName(request.getParameter("createArrivalCity"));
        Flight testFlight = flightDao.findFlightByFlightNumber(Integer.parseInt(request.getParameter("createFlightNumber")));
        if (departureCity != null && arrivalCity != null) {
            if (testFlight == null) {
                String departureDateString = request.getParameter("createDepartureDate");
                String arrivalDateString = request.getParameter("createArrivalDate");
                if (StringUtils.countMatches(departureDateString, ":") == 1) {
                    departureDateString += ":00";
                }
                if (StringUtils.countMatches(arrivalDateString, ":") == 1) {
                    arrivalDateString += ":00";
                }
                Timestamp departureDate = Timestamp.valueOf(departureDateString.replace("T", " "));
                Timestamp arrivalDate = Timestamp.valueOf(arrivalDateString.replace("T", " "));
                Flight flight = new Flight();
                flight.setFlightNumber(Integer.parseInt(request.getParameter("createFlightNumber")));
                flight.setAirplaneType(request.getParameter("createAirplaneType"));
                flight.setArrivalCity(arrivalCity);
                flight.setDepartureCity(departureCity);
                flight.setArrivalDate(arrivalDate);
                flight.setDepartureDate(departureDate);
                flightDao.addFlight(flight);
                printWriter.println("<H2> Flight Successfully added! </H2>");
            } else {
                printWriter.println("<H2> A flight already exists with that flight number!</H2>");
            }
        } else {
            printWriter.println("<H2> No cities were found with that name!</H2>");
        }
        printWriter.println("<form action=\"/admin.html\"> " +
                " <input type = \"submit\" value = \"Go Back\" />" +
                "</form>");
        printWriter.println("</body>" +
                "</html>");
    }

    private void deleteFlight(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int flightNumber = Integer.parseInt(req.getParameter("deleteFlightNumber"));
        PrintWriter printWriter = resp.getWriter();
        printWriter.println(DOCTYPE);
        printWriter.println("<html>\n" +
                "<head>" +
                "</head>" +
                "<body>");
        if (flightDao.findFlightByFlightNumber(flightNumber) != null) {
            flightDao.deleteFlightWithFlightNumber(flightNumber);
            printWriter.println("<H2> Flight successfully deleted! </H2>");
        } else {
            printWriter.println("<H2> No flights were found with that flight number </H2>");
        }
        printWriter.println("<form action=\"/admin.html\"> " +
                " <input type = \"submit\" value = \"Go Back\" />" +
                "</form>");
        printWriter.println("</body>" +
                "</html>");
    }

    private void updateFlight(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int flightNumber = Integer.parseInt(request.getParameter("createFlightNumber"));
        Flight flight = flightDao.findFlightByFlightNumber(flightNumber);
        PrintWriter printWriter = response.getWriter();
        printWriter.println(DOCTYPE);
        printWriter.println("<html>\n" +
                "<head>" +
                "</head>" +
                "<body>");
        if (flight != null) {
            flightDao.updateFlight(flight);
            printWriter.println("<H2> Flight was successfully updated! </H2>");
        } else {
            printWriter.println("<H2> No flights were found with that flight number </H2>");
        }
        printWriter.println("<form action=\"/admin.html\"> " +
                " <input type = \"submit\" value = \"Go Back\" />" +
                "</form>");
        printWriter.println("</body>" +
                "</html>");
    }

    private void displayAdministrationForms(PrintWriter printWriter) {
        printWriter.println("\n" +
                "    <H2> Welcome back!</H2>\n" +
                "\n" +
                "    <div id = \"create\">\n" +
                "        <form action = \"/admin\" method = \"POST\">\n" +
                "            <p> Add a flight:</p>\n" +
                "            <label for = \"createFlightNumber\"> Flight Number</label>\n" +
                "                <input name = \"createFlightNumber\" type=\"number\" id = \"createFlightNumber\">\n" +
                "\n" +
                "            <label for = \"createAirplaneType\"> Flight Type</label>\n" +
                "                <input name = \"createAirplaneType\" type = \"text\" id = \"createAirplaneType\">\n" +
                "\n" +
                "            <label for = \"createDepartureCity\"> Departure City</label>\n" +
                "                <input name = \"createDepartureCity\" type=\"text\" id = \"createDepartureCity\">\n" +
                "\n" +
                "            <label for = \"createArrivalCity\"> Arrival City</label>\n" +
                "                <input name = \"createArrivalCity\" type = \"text\" id = \"createArrivalCity\">\n" +
                "\n" +
                "            <label for = \"createDepartureDate\"> Departure Date</label>\n" +
                "                <input name = \"createDepartureDate\" type = \"datetime-local\" id = \"createDepartureDate\">\n" +
                "\n" +
                "            <label for = \"createArrivalDate\"> Arrival Date</label>\n" +
                "                <input name = \"createArrivalDate\" type = \"datetime-local\" id = \"createArrivalDate\">\n" +
                "\n" +
                "                <input type = \"submit\" id = \"createFlight\" name = \"createFlight\" value = \"Create Flight\">\n" +
                "                <input type = \"submit\" id = \"updateFlight\" name = \"updateFlight\" value = \"Update Flight\">\n" +
                "        </form>\n" +
                "    </div>\n" +
                "\n" +
                "    <div id = \"find\">\n" +
                "        <form action = \"/admin\" method = \"GET\">\n" +
                "            <p> Find a flight:</p>\n" +
                "            <label for = \"findFlightNumber\"> <b> Flight Number </b> </label>\n" +
                "                <input name = \"findFlightNumber\" type = \"number\" id = \"findFlightNumber\">\n" +
                "            <div class = \"button\">\n" +
                "                <input type = \"submit\" id = \"findFlight\" name = \"findFlight\" value = \"Find Flight\">\n" +
                "            </div>\n" +
                "        </form>\n" +
                "    </div>\n" +
                "\n" +
                "    <div id = \"delete\">\n" +
                "        <form action = \"/admin\" method = \"POST\">\n" +
                "            <p> Delete a flight:</p>\n" +
                "            <label for = \"deleteFlightNumber\"> <b> Flight Number </b> </label>\n" +
                "                <input name = \"deleteFlightNumber\" type = \"number\" id = \"deleteFlightNumber\">\n" +
                "            <div class = \"button\">\n" +
                "                <input type = \"submit\" id = \"deleteFlight\" name = \"deleteFlight\" value = \"Delete Flight\">\n" +
                "            </div>\n" +
                "        </form>\n" +
                "    </div>");
    }

    @Override
    public void destroy() {

    }
}
