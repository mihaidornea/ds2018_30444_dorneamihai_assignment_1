package ro.mihaidornea.facultate.ds.assignment1_2.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;

public class HtmlUtils {

    public static void printErrorPage(PrintWriter printWriter) {
        printWriter.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Title</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "    <H1>ACCESS DENIED - Please log in with an account with appropriate access rights</H1>\n" +
                "\n" +
                "<form method=\"get\" action=\"/login\">\n" +
                "    <input type=\"submit\" name=\"goToLogin\" id=\"goToLogin\" value=\"Login\">\n" +
                "</form>" +
                "</body>\n" +
                "</html>");
    }

    public static boolean hasAppropriateRole(HttpServletRequest request, String role){
        Cookie[] cookies = request.getCookies();
        String userRole = "";
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("role"))
                userRole = cookie.getValue();
        }
        HttpSession httpSession = request.getSession(false);
        if (httpSession != null) {
            userRole = (String) httpSession.getAttribute("role");
        }
        return userRole.equals(role);
    }
}
