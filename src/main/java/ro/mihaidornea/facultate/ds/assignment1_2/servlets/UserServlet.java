package ro.mihaidornea.facultate.ds.assignment1_2.servlets;

import com.sun.deploy.net.HttpRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Configuration;
import org.json.JSONObject;
import ro.mihaidornea.facultate.ds.assignment1_2.dao.FlightDao;
import ro.mihaidornea.facultate.ds.assignment1_2.dao.UserDao;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.City;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.Flight;
import ro.mihaidornea.facultate.ds.assignment1_2.utils.HtmlUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class UserServlet extends HttpServlet {

    private static final String DOCTYPE = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
    private static final String USER_ROLE = "USER";
    private FlightDao flightDao;

    @Override
    public void init() throws ServletException {
        super.init();
        flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter printWriter = response.getWriter();
        if (HtmlUtils.hasAppropriateRole(request, USER_ROLE)) {
            printWriter.println(DOCTYPE);
            printWriter.println("<html>\n" +
                    "<head>" +
                    //"    <LINK rel = \"stylesheet\" href = \"css/list.css\" type = \"text/css\">\n" +
                    "</head>" +
                    "<body>");
            printWriter.println(" <form action=\"/user\" method=\"GET\">\n" +
                    "        <input type=\"submit\" value=\"See flights\" + name=\"listFlights\">\n" +
                    "    </form>");
            if (request.getParameter("listFlights") != null || request.getParameter("city") != null) {
                List<Flight> flightList = flightDao.listFlights();
                printWriter.println("<ul>");
                for (Flight flight : flightList) {
                    Date departureDate = new Date(flight.getDepartureDate().getTime());
                    Date arrivalDate = new Date(flight.getArrivalDate().getTime());
                    printWriter.println("<li>\n" +
                            "<H2> Flight no: " + flight.getFlightNumber() + "\n" + "</H2>" +
                            "<b>Departure City:</b> " +
                                    "<form method=\"get\" action=\"/user\">\n" +
                                    "    <input type=\"submit\" name=\"city\" id=\"city\" value=\"" + flight.getDepartureCity().getName() + "\">\n" +
                                    "</form>\n");
                    if (request.getParameter("city") != null) {
                        if (request.getParameter("city").equals(flight.getDepartureCity().getName())) {
                            printWriter.println("<b> Local Time: </b>" + getLocalTime(flight.getDepartureCity(), flight.getDepartureDate().getTime()).toString() + "<p></p>");
                        }
                    }
                    printWriter.println(
                            "<b>Departure Date:</b> " + departureDate.toString() + "<br>\n" +
                            "<p></p>" +
                            "<b>Arrival City:</b> " +
                                    "<form method=\"get\" action=\"/user\">\n" +
                                    "    <input type=\"submit\" name=\"city\" id=\"city\" value=\"" + flight.getArrivalCity().getName() + "\">\n" +
                                    "</form>\n");
                    if (request.getParameter("city") != null) {
                        if (request.getParameter("city").equals(flight.getArrivalCity().getName())) {
                            printWriter.println("<b> Local Time: </b>" + getLocalTime(flight.getDepartureCity(), flight.getArrivalDate().getTime()).toString() + "<p></p>");
                        }
                    }
                    printWriter.println(
                            "<b>Arrival Date:</b> " + arrivalDate.toString() + "<br>\n" +
                            "<p></p>" +
                            "<b>Airplane Type:</b> " + flight.getAirplaneType() + "<br>\n" +
                            "</li>" + "\n"
                    );
                }
                printWriter.println("</ul>\n");
            }
            printWriter.println("</body>" +
                    "</html>");

        } else {
            HtmlUtils.printErrorPage(printWriter);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

    }

    private Timestamp getLocalTime(City city, long time){
        int offset = 0;
        try {
            StringBuilder stringBuilder = new StringBuilder();
            URL url = new URL("http://api.geonames.org/timezoneJSON?lat=" + city.getLatitude() + "&lng=" +city.getLongitude() + "&time=" + time + "&username=mihaidornea");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String incomingMessage;
            while ((incomingMessage = reader.readLine()) != null){
                stringBuilder.append(incomingMessage);
            }
            reader.close();
            JSONObject result = new JSONObject(stringBuilder.toString());
            offset = result.optInt("dstOffset");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Timestamp(time+offset*60*60*1000);
    }

    @Override
    public void destroy() {

    }
}
