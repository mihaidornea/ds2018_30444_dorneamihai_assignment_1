package ro.mihaidornea.facultate.ds.assignment1_2.entities;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name = "flight")
@Getter @Setter @NoArgsConstructor @AllArgsConstructor //LOMBOK ANNOTATIONS
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int flightNumber;
    private String airplaneType;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "departureCity", referencedColumnName = "id")
    private City departureCity;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "arrivalCity", referencedColumnName = "id")
    private City arrivalCity;
    private Timestamp departureDate;
    private Timestamp arrivalDate;

    /**
     * IN CASE LOMBOK PLUGIN IS NOT SUPPORTED IN YOUR IDE, UNCOMMENT THE FOLLOWING LINES
     */

    /*
    public Flight() {
    }

    public Flight(int flightNumber, String airplaneType, City departureCity, City arrivalCity, Timestamp departureDate, Timestamp arrivalDate) {
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Timestamp getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Timestamp departureDate) {
        this.departureDate = departureDate;
    }

    public Timestamp getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Timestamp arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
    */
}
