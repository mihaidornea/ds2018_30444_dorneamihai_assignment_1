package ro.mihaidornea.facultate.ds.assignment1_2.utils;

import java.util.Base64;

public class CryptoUtils {

    public static String encode(String password){
        byte[]   bytesEncoded = Base64.getEncoder().encode(password.getBytes());
        return new String(bytesEncoded);
    }
}
