package ro.mihaidornea.facultate.ds.assignment1_2;

import junit.framework.TestCase;
import org.hibernate.cfg.Configuration;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import ro.mihaidornea.facultate.ds.assignment1_2.dao.CityDao;
import ro.mihaidornea.facultate.ds.assignment1_2.dao.FlightDao;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.City;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.Flight;

import java.sql.Timestamp;
import java.util.Date;

public class FlightDaoTest extends TestCase {

    private FlightDao flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());

    @AfterEach
    public void tearDown(){
        flightDao.deleteAllFlights();
    }

    private City createCity(String name, float lat, float lng){
        CityDao cityDao = new CityDao(new Configuration().configure().buildSessionFactory());
        City city = new City();
        city.setName(name);
        city.setLongitude(lng);
        city.setLatitude(lat);
        city = cityDao.addCity(city);
        return city;
    }

    private Flight createFlight(){
        City departureCity = createCity("Milano", 123f, 123f);
        City arrivalCity = createCity("Amsterdam", 132f, 132f);
        Flight flight = new Flight();
        flight.setAirplaneType("Cruise");
        flight.setFlightNumber(123);
        flight.setDepartureCity(departureCity);
        flight.setArrivalCity(arrivalCity);
        flight.setDepartureDate(new Timestamp(new Date().getTime()));
        flight.setArrivalDate(new Timestamp(new Date().getTime()));
        flight = flightDao.addFlight(flight);
        return flight;
    }



    @Test
    public void testFindFlight(){
        Flight flight = createFlight();
        assertNotNull(flightDao.findFlight(flight.getId()));
    }

    @Test
    public void testFindFlightByFlightNumber(){
        Flight flight = createFlight();
        assertNotNull(flightDao.findFlightByFlightNumber(flight.getFlightNumber()));
    }

    @Test
    public void testCreateFlight(){
        Flight flight = createFlight();
        assertFalse(flightDao.listFlights().isEmpty());
    }

    @Test
    public void testDeleteFlight(){
        Flight flight = createFlight();
        flightDao.deleteFlight(flight.getId());
        assertNull(flightDao.findFlight(flight.getId()));
    }

    @Test
    public void testDeleteAllFlights(){
        assertTrue(flightDao.listFlights().isEmpty());
    }

}
