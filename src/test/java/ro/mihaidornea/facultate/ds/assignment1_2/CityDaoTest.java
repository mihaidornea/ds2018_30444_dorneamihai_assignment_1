package ro.mihaidornea.facultate.ds.assignment1_2;

import junit.framework.TestCase;
import org.hibernate.cfg.Configuration;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import ro.mihaidornea.facultate.ds.assignment1_2.dao.CityDao;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.City;

public class CityDaoTest extends TestCase {

    private final CityDao cityDao = new CityDao(new Configuration().configure().buildSessionFactory());

    @AfterEach
    public void tearDown(){
        cityDao.deleteAllCities();
    }

    private City createCity(){
        City city = new City();
        city.setName("Amsterdam");
        city.setLongitude(11f);
        city.setLatitude(12f);
        city = cityDao.addCity(city);
        return city;
    }

    @Test
    public void testCityAddAndFind(){
        City city = createCity();
        assertEquals(cityDao.findCityById(city.getId()).getName(), "Amsterdam");
    }

    @Test
    public void testCityFindByName(){
        City city = createCity();
        assertEquals(cityDao.findCityByName("Amsterdam").getId(), city.getId());
    }

    @Test
    public void testCityDelete(){
        City city = createCity();
        cityDao.deleteCity(city.getId());
        assertNull(cityDao.findCityById(city.getId()));
    }

    @Test
    public void testCityAdd(){
        City city = createCity();
        assertNotNull(cityDao.findCityById(city.getId()));
    }

    @Test
    public void testCityDeleteAll(){
        assertTrue(cityDao.listCities().isEmpty());
    }
}
