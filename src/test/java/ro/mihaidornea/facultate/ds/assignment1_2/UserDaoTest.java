package ro.mihaidornea.facultate.ds.assignment1_2;

import junit.framework.TestCase;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.jupiter.api.AfterEach;
import ro.mihaidornea.facultate.ds.assignment1_2.dao.UserDao;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.User;

public class UserDaoTest extends TestCase {

    private UserDao userDao = new UserDao(new Configuration().configure().buildSessionFactory());

    @AfterEach
    public void tearDown(){
        userDao.deleteAllUsers();
    }

    private User createUser(){
        User user = new User();
        user.setRole("ADMIN");
        user.setPassword("ayyyylmao");
        user.setUsername("dsaoi");
        user = userDao.addUser(user);
        return user;
    }


    public void testUserAdd(){
        User user = createUser();
        assertFalse(userDao.listUsers().isEmpty());
    }

    public void testUserFind(){
        User user = createUser();
        assertNotNull(userDao.findUser(user.getId()));
    }

    public void testUserFindByUsername(){
        User user = createUser();
        assertNotNull(userDao.findUser(user.getUsername()));
    }

    public void testUserDelete(){
        User user = createUser();
        userDao.deleteUser(user.getUsername());
        assertNull(userDao.findUser(user.getUsername()));
    }

    public void testUserDeleteAll(){
        assertTrue(userDao.listUsers().isEmpty());
    }


}
